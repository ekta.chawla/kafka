#PROVIDER

provider "aws" {
  version    = "~> 2.0"
  region     = var.aws_region
  access_key = var.accesskey
  secret_key = var.secretkey
}

#VPC

resource "aws_vpc" "myvpc" {
  cidr_block = var.vpc_cidr

tags = {
    Name = "MyVPC"
  }
}

#SUBNETS

resource "aws_subnet" "public" {
  vpc_id     = aws_vpc.myvpc.id
  cidr_block = var.public_cidr
  availability_zone = var.publiczone
tags = {
    Name = "Public Subnet"
  }
}

resource "aws_subnet" "private" {
  vpc_id     = aws_vpc.myvpc.id
  cidr_block = var.private_cidr
  availability_zone = var.privatezone

tags = {
    Name = "Private Subnet"
  }
}

#Internet Gateway

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.myvpc.id
tags = {
    Name = "IGW"
  }
}

#Elastic IP

resource "aws_eip" "nat" {
  vpc      = true
}

#NAT Gateway

resource "aws_nat_gateway" "ngw" {
  allocation_id = aws_eip.nat.id
  subnet_id     = aws_subnet.public.id


tags = {
    Name = "NAT"
  }
}


# Public Route Table

resource "aws_route_table" "public_route" {
    vpc_id = aws_vpc.myvpc.id
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.gw.id
    }

    tags = {
        Name = "Public_Route"
    }
}


# Private Route Table


resource "aws_route_table" "private_route" {
    vpc_id = aws_vpc.myvpc.id
    route {
        cidr_block = "0.0.0.0/0"
        nat_gateway_id = aws_nat_gateway.ngw.id
}

    tags = {
        Name = "Private_Route"
    }
}

# Public Subnet Association with Route Table

resource "aws_route_table_association" "pubroute" {
    subnet_id = aws_subnet.public.id
    route_table_id = aws_route_table.public_route.id
}

# Private Subnet association with Route Table

resource "aws_route_table_association" "priroute" {
    subnet_id = aws_subnet.private.id
    route_table_id = aws_route_table.private_route.id
}



# Security Group for EC2


resource "aws_security_group" "allow_ssh" {
  name        = "allow_ssh"
  description = "Allow ssh connection"
  vpc_id      = aws_vpc.myvpc.id

  ingress {
    description = "For ssh connection"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }


    ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_ssh"
  }
}


# EC2 intance in Public Subnet

resource "aws_instance" "public_server" {
  ami = var.ami_image
  subnet_id = aws_subnet.public.id
  instance_type = var.instance_type
  key_name = "deployer-key"
  associate_public_ip_address = "true"
  security_groups = [aws_security_group.allow_ssh.id]



provisioner "local-exec" {
    command = " ansible-playbook -i ec2.py kafkarole.yml -u ubuntu --ssh-common-args='-o StrictHostKeyChecking=no' "


  }

  provisioner "remote-exec" {
    inline = [
       "sudo apt-get update && sudo apt-get install -y python" ]
}

  connection {
    type     = "ssh"
    user     = "ubuntu"
    password = ""
    private_key = "${file("~/.ssh/id_rsa")}"
    host = "${aws_instance.public_server.public_ip}"
}

tags = {
    Name = "Public_Server"
 }
}


# Key Pair

resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = var.publickey

}

#Adding S3 bucket 

terraform {
  backend "s3" {
    bucket = "ekta-terraform-kafka"
    key    = "terraform.tfstate"
    region = "us-east-1"
  }
}


# EC2 instance for Private Subnet

resource "aws_instance" "private_server" {
  ami = var.ami_image
  subnet_id = aws_subnet.private.id
  instance_type = var.instance_type
  key_name = "deployer-key"
  associate_public_ip_address = "false"
  security_groups = [aws_security_group.allow_ssh.id]
tags = {
    Name = "Private_Server"
}
}









