Kafka Read Me

Create a topic (Gets created by Kafka Role - Automated)

create a topic named "test" with a single partition and only one replica:
1
	
> /usr/local/kafka/kafka_2.12-2.5.0/bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic test

Producer : Send some messages

Kafka comes with a command line client that will take input from a file or from standard input and send it out as messages to the Kafka cluster. By default, each line will be sent as a separate message.

Run the producer and then type a few messages into the console to send to the server.
1
2
3
	
> /usr/local/kafka/kafka_2.12-2.5.0/bin/kafka-console-producer.sh --bootstrap-server localhost:9092 --topic test
This is a message
This is another message


Consumer : Start a consumer

Kafka also has a command line consumer that will dump out messages to standard output.
1
2
3
	
> /usr/local/kafka/kafka_2.12-2.5.0/bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic test --from-beginning
This is a message
This is another message

If you have each of the above commands running in a different terminal then you should now be able to type messages into the producer terminal and see them appear in the consumer terminal. 

